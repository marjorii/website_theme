// check if css disabled
let testcss = document.createElement('div');
testcss.style.position = 'absolute';
document.getElementsByTagName('body')[0].appendChild(testcss);
if (testcss.currentStyle) currstyle = testcss.currentStyle['position'];
else if (window.getComputedStyle) currstyle = document.defaultView.getComputedStyle(testcss, null).getPropertyValue('position');
cssdisabled = (currstyle == 'static') ? true : false;
document.getElementsByTagName('body')[0].removeChild(testcss);

const audio = document.querySelector('audio');
const links = document.querySelectorAll('main a');

const div = document.createElement('div');

window.addEventListener("DOMContentLoaded", (event) => {
    if (window.location.hash !== '') {
        const hash = String(window.location.hash.substring(1));
        if (hash == 'start') {
            window.scrollTo({ top: 0, behavior: 'auto' });
            setTimeout(function() {
                document.querySelector('h1').focus();
            }, 100);
        }
    }
});

// mobile menu
const menuButton = document.getElementById('menu');
const menu = document.getElementById('pages');

menuButton.addEventListener("click", function(e) {
    if (menu.classList.contains('hide')) {
        this.setAttribute('aria-expanded', 'true');
    }
    else {
        this.setAttribute('aria-expanded', 'false');
    }
    menu.classList.toggle('hide');
    e.preventDefault();
    e.stopPropagation();
})

// close mobile menu on some elems focus
menuButton.onfocus = (e) => {
    if (!menu.classList.contains('hide')) {
        setTimeout(() => {
            if (!menu.classList.contains('hide')) {
                hideMenu();
            }
        }, 100)
    }
}

const accessMode = localStorage.getItem('accessible-mode') === 'true';

if (accessMode || window.innerWidth < 767 || window.innerHeight < 767) {
    document.querySelector('#top-accessible-banner li:last-of-type a').onfocus = () => {
        if (!menu.classList.contains('hide')) {
            hideMenu();
        }
    }
}

if (accessMode || window.innerWidth < 767 || window.innerHeight < 767) {
    setTimeout(() => {
        menu.lastElementChild.firstElementChild.onblur = (e) => {
            if (e.relatedTarget != menu.lastElementChild.previousElementSibling.firstElementChild) {
                hideMenu();
            }
        }
    }, 100);
}

// close mobile menu on window click
window.addEventListener('click', function(e) {
    hideMenu();
});

// close mobile menu on ESC key press
window.addEventListener('keyup', function(event) {
    if (event.keyCode == 27) {
        const menuArray = Array.from(document.querySelectorAll('#menu, #pages > li > a'));
        if (!menu.classList.contains('hide') && menuArray.some(elem => elem == document.activeElement)) {
                hideMenu();
                menuButton.focus();
        }
    }
});

function hideMenu() {
    if (!menu.classList.contains('hide')) {
        menu.classList.add('hide');
        menuButton.setAttribute('aria-expanded', 'false');
    }
}

// spell button
if (!cssdisabled) {
    document.getElementById('spell-button').onclick = function() {
        document.body.classList.toggle('glossy');
        if (!audio.paused) {
            audio.pause();
            audio.currentTime = 0;
        }
        audio.volume = 0.25;
        audio.play();
        if (!document.body.classList.contains('glossy')) {
            div.classList.add('hidden');
            this.setAttribute('aria-pressed', 'false');
            if (div.children.length >= 1) {
                Array.from(div.children).forEach((star) => {
                    star.remove();
                });
            }
        }
        else {
            this.setAttribute('aria-pressed', 'true');
            glitter();
            div.classList.remove('hidden');
        }
    }
}

// accessible mode
const accessModeBtn = document.getElementById('accessible-mode-btn');

accessModeBtn.onclick = () => {
    document.body.classList.toggle('accessible-mode');
    switchMode();
    if (document.body.classList.contains('glossy')) {
        document.body.classList.remove('glossy');
        div.classList.add('hidden');
    }
    location.reload();
}

if (localStorage.getItem('accessible-mode') === 'true') {
    switchMode(true)
}

function switchMode(force) {
    const darkmode = localStorage.getItem('accessible-mode') === 'true'
    localStorage.setItem('accessible-mode', force || !darkmode)

    if (force || !darkmode) {
        document.body.classList.add('accessible-mode');
        accessModeBtn.setAttribute('aria-pressed', 'true');
    }
    else {
        document.body.classList.remove('accessible-mode');
        accessModeBtn.setAttribute('aria-pressed', 'false');
    }
}


// back to top button
const topBtn = document.getElementById('top-btn');
topBtn.onclick = (e) => {
    e.preventDefault();
    window.scrollTo({ top: 0, behavior: 'smooth' });
    setTimeout(function() {
        location.hash = topBtn.hash;
        document.querySelector('h1').focus();
            window.scrollTo({ top: 0, behavior: 'smooth' });
    }, 100);
}

// On top or bottom banner buttons focus, scrollTo banner
document.querySelectorAll('#accessible-mode-btn, #top-accessible-banner li:last-of-type a').forEach((elem) => {
    elem.addEventListener('focus', (e) => {
        if (document.firstElementChild.scrollTop != 0) {
            window.scrollTo({top: 0, behavior: 'auto' });
        }
    });
});

[topBtn, document.querySelector('#bottom-accessible-banner li:last-of-type a')].forEach((elem) => {
    elem.addEventListener('focus', (e) => {
        if ((document.firstElementChild.scrollTop + window.innerHeight) != document.firstElementChild.scrollHeight) {
            window.scrollTo({top: document.firstElementChild.scrollHeight, behavior: 'auto' });
        }
    });
});

// Glitter effect (original code by Dimpu)
function glitter() {
    let limit = 100, // Max number of stars
    loop = {
        //initilizeing
        start: function() {
            for (var i = 0; i <= limit; i++) {
                let star = this.newStar();
                star.style.top = this.rand() * 100 + '%';
                star.style.left = this.rand() * 100 + '%';
                star.style.webkitAnimationDelay = this.rand() + 's';
                star.style.mozAnimationDelay = this.rand() + 's';

                div.appendChild(star);
                div.setAttribute('aria-hidden', 'true');
                div.setAttribute('id', 'stars-container');
                div.classList.add('hidden');
                document.body.appendChild(div);
            }
        },
        //to get random number
        rand: function() {
            return Math.random();
        },
        //creating html dom for star
        newStar: function() {
            let d = document.createElement('div');
            d.innerHTML =
            '<figure class="star">⋆</figure>';
            return d.firstChild;
        },
    };
    loop.start();
}
