const years = document.querySelectorAll('.time');
yearsArr = Array.from(years);

yearsArr.forEach((event) => {
    ariaCheck(event, event.parentElement.nextElementSibling);
    event.onclick = (e) => {
        event.parentElement.nextElementSibling.classList.toggle('hidden');
        ariaCheck(event, event.parentElement.nextElementSibling);
    }
});

function ariaCheck(button, elem) {
    if (!elem.classList.contains('hidden')) {
        button.setAttribute('aria-expanded', 'true');
    }
    else {
        button.setAttribute('aria-expanded', 'false');
    }
}
