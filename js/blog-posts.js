document.querySelector('#post-pages-menu input').onclick = function(e) {
    let ariaExpanded = this.getAttribute('aria-expanded');
    const ariaLabel = this.getAttribute('aria-label');
    document.getElementById('post-pages-list').classList.toggle('hidden');
    if ( ariaExpanded == 'false') {
        this.setAttribute('aria-expanded', 'true');
    }
    else {
        this.setAttribute('aria-expanded', 'false');
    }
    e.preventDefault();
    e.stopPropagation();
}

document.querySelector('#post-pages-menu input').addEventListener('focus', () => {
    if (window.innerWidth < 767 || window.innerHeight < 767 || accessMode) {    
        if (!menu.classList.contains('hide')) {
            hideMenu();
        }
    }
});

document.querySelectorAll('.post-page').forEach(function(page) {

    page.firstElementChild.onclick = function(e) {
        const prevUl = document.querySelector('.articles:not(.hidden)');

        if (prevUl) {
            prevUl.classList.toggle('hidden');
            prevUl.previousElementSibling.setAttribute('aria-expanded', 'false');
            if (prevUl.parentElement != prevUl.parentElement.parentElement.firstElementChild && !accessMode && (window.innerWidth >= 767 || window.innerWidth > window.innerHeight)) {
                prevUl.parentElement.classList.toggle('correct-margin');
            }
            const currentPage = prevUl.parentElement.nextElementSibling;
            if (currentPage) {
                currentPage.firstElementChild.classList.remove('stroke');
            }
            if (prevUl.previousElementSibling == this) {
                return;
            }
        }
        page.lastElementChild.classList.remove('hidden');
        this.setAttribute('aria-expanded', 'true');
        if (page != page.parentElement.firstElementChild && !accessMode && (window.innerWidth >= 767 || window.innerWidth > window.innerHeight)) {
            page.classList.add('correct-margin');
        }
        // page.nextElementSibling = next li
        if (page.nextElementSibling) {
            page.nextElementSibling.firstElementChild.classList.add('stroke');
        }
        e.preventDefault();
        e.stopPropagation();
    }

    if (accessMode || (window.innerWidth < 767 && window.innerWidth < window.innerHeight)) {
        page.classList.remove('correct-margin');
    }

    // On focus

    page.firstElementChild.addEventListener('focus', (e) => {
        if (page.nextElementSibling && !page.nextElementSibling.lastElementChild.classList.contains('hidden')) {
            closeSearchPages(page.nextElementSibling);
        }
        if (page.previousElementSibling && !page.previousElementSibling.lastElementChild.classList.contains('hidden')) {
            setTimeout(() => {
                 closeSearchPages(page.previousElementSibling);
            }, 100)
        }
    });

});

// hide search menu on menu button, search menu button or first list item focus
document.querySelectorAll('#menu, #post-pages-menu input, #list li:first-of-type button').forEach((elem) => {
    elem.addEventListener('focus', (e) => {
        if (!document.getElementById('post-pages-list').classList.contains('hidden')) {
            setTimeout(() => {
                 closeSearchMenu();
            }, 100)
        }
        if (document.querySelector('.articles:not(.hidden)')) {
            setTimeout(() => {
                closeSearchPages(document.querySelector('.articles:not(.hidden)').parentElement);
            }, 100)
        }
    });
});

window.addEventListener('resize', () => {
    if (document.querySelector('.articles:not(.hidden)')) {
        if (accessMode || (window.innerWidth < 767 && window.innerWidth < window.innerHeight)) {
            document.querySelectorAll('.post-page').forEach(function(page) {
                page.classList.remove('correct-margin');
            });
        }
        else {
            if (document.querySelector('.articles:not(.hidden)').parentElement != document.querySelector('.articles:not(.hidden)').parentElement.parentElement.firstElementChild) {
                document.querySelector('.articles:not(.hidden)').parentElement.classList.add('correct-margin');
            }
        }
    }
});

window.addEventListener('keyup', function(event) {
    if (event.keyCode == 27) {
        const searchMenuArray = Array.from(document.querySelectorAll('#post-pages-menu > nav > input, .post-page button'));
        const listsMenuArray = Array.from(document.querySelectorAll('.post-page ul'));
        const pagesMenuArray = Array.from(document.querySelectorAll('.post-page a'));
        if (!document.getElementById('post-pages-list').classList.contains('hidden') && searchMenuArray.some(elem => elem == document.activeElement) && listsMenuArray.every(elem => elem.classList.contains('hidden'))) {
            closeSearchMenu();
            document.querySelector('#post-pages-menu input').focus();
        }
        document.querySelectorAll('.post-page').forEach(function(page) {
            if (!page.lastElementChild.classList.contains('hidden') && (pagesMenuArray.some(elem => elem == document.activeElement) || searchMenuArray.some(elem => elem == document.activeElement))) {
                closeSearchPages(page);
                page.firstElementChild.focus();
            }
        });
    }
});

window.addEventListener('click', function(event) {
    if (!document.getElementById('post-pages-list').classList.contains('hidden') && !event.target.parentElement.classList.contains('post-page')) {
        closeSearchMenu();
    }
    document.querySelectorAll('.post-page').forEach(function(page) {
        if (!page.lastElementChild.classList.contains('hidden') && event.target.parentElement != page.firstElementChild) {
            closeSearchPages(page);
        }
    });
});

document.querySelectorAll('.item, .post-link').forEach(function(elem) {

    elem.addEventListener('focus', () => {
        if (document.firstElementChild.scrollTop > 0 && !document.body.classList.contains('scrolled') && window.innerWidth >= 767 && window.innerHeight >= 767 && !accessMode) {
            document.body.classList.add('scrolled');
            document.body.style.top = document.firstElementChild.scrollTop + 'px';
        }
    });
});

document.querySelectorAll('.item').forEach(function(elem) {

    elem.addEventListener('click', function(event) {
        const content = elem.parentElement.parentElement.lastElementChild;
        content.classList.toggle('hide');
        content.classList.toggle('flex');
        if (content.classList.contains('hide')) {
            elem.setAttribute('aria-expanded', 'false');
        }
        else {
            elem.setAttribute('aria-expanded', 'true');
        }
    });
});

// handle aside left filters
document.querySelectorAll('aside.left input').forEach(function(elem) {
    // elem.setAttribute('aria-checked', 'false');
    elem.onclick = () => {
        toggleFilter(elem);
    }
});
window.addEventListener('load', () => {
    const filter = new URLSearchParams(window.location.search).get('filter')
    if (filter) {
        const elem = document.querySelector('aside.left #' + filter);
        if (elem) {
            toggleFilter(elem);
            elem.checked = true;
        }
    }
});

function toggleFilter(elem) {
    const projects = Array.from(document.querySelectorAll('.project-list'));
    const pagination =  document.querySelectorAll('#pagination a');
    let other = document.querySelector('aside.left label.selected');
    if (other) {
        other.classList.remove('selected');
        // other.previousElementSibling.setAttribute('aria-checked', 'false');
        if (other === elem.previousElementSibling) {
            projects.forEach(function(proj) {
                proj.parentElement.classList.remove('match');
            });
            return;
        }
    }
    elem.nextElementSibling.classList.add('selected');
    // elem.setAttribute('aria-checked', 'true');
    projects.forEach(function(project) {
        if (project.dataset.type.includes(elem.dataset.type)) {
            project.parentElement.classList.remove('hide');
        }
        else {
            project.parentElement.classList.add('hide');
        }
    });

    // handle no scroll case
    if (!accessMode && scrollableElem.scrollHeight <= scrollableElem.offsetHeight) {
        document.body.classList.add('no-scroll');
    }
    else {
        if (document.body.classList.contains('no-scroll')) {
            document.body.classList.remove('no-scroll');
        }
    }

    // remove selected and aria-checked if reclick on input
    if (other && other.previousElementSibling == elem) {
        // if (elem.getAttribute('aria-checked') == false) {
        if (elem.checked = false) {
            elem.nextElementSibling.classList.add('selected');
            // elem.setAttribute('aria-checked', 'true');
            elem.checked = true;
        }
        else {
            elem.nextElementSibling.classList.remove('selected');
            // elem.setAttribute('aria-checked', 'false');
            elem.checked = false;
        }
        projects.forEach(function(project) {
            if (!project.dataset.type.includes(elem.dataset.type)) {
                project.parentElement.classList.remove('hide');
            }
        });
        if (document.body.classList.contains('no-scroll')) {
            document.body.classList.remove('no-scroll');
        }

        // handle query removal
        pagination.forEach((a) => {
            a.href = a.href.split('?')[0]
        });
        console.log(window.location)
        history.replaceState(null, null, window.location.pathname);
    } else {
        // handle query addition
        const query = new URLSearchParams({ filter: elem.id })
        history.replaceState(null, null, window.location.pathname + '?' + query);
        pagination.forEach((a) => {
            a.href = a.href.split('?')[0] + '?' + query;
        });
    }


}

// enlargeImg();

// display current page number and pages length
if (!window.location.href.includes('page:')) {
    document.querySelectorAll('footer p, #page-title h1 > span:last-of-type').forEach((elem) => {
        elem.textContent = elem.textContent + ' – page 1' + ' sur ' + document.getElementById('pagination').dataset.length;
    });
    document.querySelector('#blog-select-page a').setAttribute('title', 'Billets (page 1' + ' sur ' + document.getElementById('pagination').dataset.length + ') – Page active');
}
else {
    let pageNumber = window.location.href;
    if (window.location.hash) {
        pageNumber = pageNumber.replace(window.location.hash, '');
    }
    pageNumber = pageNumber.split('/page:', 2);
    pageNumber = parseFloat(pageNumber[1]);
    if (typeof pageNumber == 'number' && Number.isInteger(pageNumber) && pageNumber <= document.getElementById('pagination').dataset.length) {
        document.querySelectorAll('footer p, #page-title h1 > span:last-of-type').forEach((elem) => {
            elem.textContent = elem.textContent + ' – page ' + pageNumber + ' sur ' + document.getElementById('pagination').dataset.length;
        });
        document.querySelector('#blog-select-page a').setAttribute('title', 'Billets (page ' + pageNumber + ' sur ' + document.getElementById('pagination').dataset.length + ') – Page active');

    }

    // FIXME /!\ Can't load error page (want to handle the case one enters manually a number, higher than number of pages for example)

    else {
        // load error page (page not found), only clue I have = body has error class, no infos in location or document elem found.
    }
}

function closeSearchMenu() {
    document.getElementById('post-pages-list').classList.add('hidden');
    document.querySelector('#post-pages-menu input').setAttribute('aria-expanded', 'false');
}

function closeSearchPages(page) {
    if (!page.lastElementChild.classList.contains('hidden')) {
        page.lastElementChild.classList.add('hidden');
        page.firstElementChild.setAttribute('aria-expanded', 'false');
        if (page.nextElementSibling) {
            page.nextElementSibling.firstElementChild.classList.remove('stroke');
        }
        if (page != page.parentElement.firstElementChild) {
            page.classList.remove('correct-margin');
        }
    }
}

// handle media description (show/hide)
document.querySelectorAll('.media-container').forEach((elem) => {
    if (elem.nextElementSibling) {
        if (elem.nextElementSibling.nodeName == 'FIGCAPTION' && elem.nextElementSibling.textContent) {
            elem.nextElementSibling.classList.add('extra-top-margin');
        }
    }
});

function showMediaDesc() {
    const button = event.target.parentElement.querySelector('.media-button');
    button.nextElementSibling.classList.toggle('hide');
    let buttonValue = button.firstChild.textContent;
    const mediaName = button.firstElementChild.textContent;
    if (button.nextElementSibling.classList.contains('hide')) {
        button.setAttribute('aria-expanded', 'false');
        if (document.firstElementChild.lang == 'en') {
            buttonValue = buttonValue.replace('Hide', 'Display');
        }
        else {
            buttonValue = buttonValue.replace('Cacher', 'Afficher');
        }
    }
    else {
        button.setAttribute('aria-expanded', 'true');
        if (document.firstElementChild.lang == 'en') {
            buttonValue = buttonValue.replace('Display', 'Hide');
        }
        else {
            buttonValue = buttonValue.replace('Afficher', 'Cacher');
        }
    }
    button.textContent = buttonValue;
    const mediaSpan = document.createElement('span');
    mediaSpan.classList.add('sr-only');
    mediaSpan.textContent = mediaName;
    button.append(mediaSpan);
}
