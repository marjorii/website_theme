// handle hash and scroll behavior
window.addEventListener("DOMContentLoaded", (event) => {
    if (window.location.hash !== '') {
        const hash = String(window.location.hash.substring(1));
        if (hash == 'content' || hash == 'menu' || hash == 'aside') {
            let a11yBannerElems = Array.from(document.querySelectorAll('#top-accessible-banner a'));
            a11yBannerElems = a11yBannerElems.filter(hasHash);
            if (a11yBannerElems.some(elem => elem.hash.substring(1) == hash)) {
                anchorClick(a11yBannerElems.find(elem => elem.hash.substring(1) == hash));
            }
        }
        else if (hash == 'start') {

            window.scrollTo({ top: 0, behavior: 'auto' });
            setTimeout(function() {
                document.querySelector('h1').focus();
            }, 100);
        }
    }
    setTimeout(scrollCheck, 300)
});

// publications button
const publications = Array.from(document.querySelectorAll('#blog-pages-list-container a'));
let pathname = window.location.pathname;
if (pathname.includes('posts')) {
    publications[0].classList.add('selected');
}
else if (pathname.includes('texts')) {
    publications[1].classList.add('selected');
}

// handle blog menu
const blogMenuButton = document.getElementById('blog-pages-list-button');

if (accessMode || window.innerWidth < 767 || window.innerHeight < 767) {
    if ((blogMenuButton.nextElementSibling.querySelector('li:first-of-type a').classList.contains('selected') || blogMenuButton.nextElementSibling.querySelector('li:last-of-type a').classList.contains('selected')) && blogMenuButton.nextElementSibling.classList.contains('hide')) {
        blogMenuButton.setAttribute('aria-expanded', 'true');
        blogMenuButton.classList.add('focus');
        blogMenuButton.nextElementSibling.classList.remove('hide')
    }
}

// Add aria-selected to posts or texts item in menu if page is active
if (blogMenuButton.nextElementSibling.querySelector('li:first-of-type a').classList.contains('selected')) {
    blogMenuButton.nextElementSibling.querySelector('li:first-of-type a').setAttribute('aria-selected', 'true');
    if (blogMenuButton.nextElementSibling.querySelector('li:last-of-type a').hasAttribute('aria-selected')) {
        blogMenuButton.nextElementSibling.querySelector('li:lasst-of-type a').removeAttribute('aria-selected');
    }
}
else if (blogMenuButton.nextElementSibling.querySelector('li:last-of-type a').classList.contains('selected')) {
    blogMenuButton.nextElementSibling.querySelector('li:last-of-type a').setAttribute('aria-selected', 'true');
    if (blogMenuButton.nextElementSibling.querySelector('li:first-of-type a').hasAttribute('aria-selected')) {
        blogMenuButton.nextElementSibling.querySelector('li:first-of-type a').removeAttribute('aria-selected');
    }
}

blogMenuButton.onclick = function(e) {
    if (blogMenuButton.nextElementSibling.classList.contains('hide')) {
        blogMenuButton.setAttribute('aria-expanded', 'true');
        blogMenuButton.classList.add('focus');
        blogMenuButton.nextElementSibling.classList.remove('hide');
    }
    else {
        blogMenuButton.setAttribute('aria-expanded', 'false');
        blogMenuButton.classList.remove('focus');
        blogMenuButton.nextElementSibling.classList.add('hide');
    }

    e.preventDefault();
    e.stopPropagation();
}

// close second menu on some elems focus
if ((window.innerWidth >= 767 && window.innerHeight >= 767 && !accessMode) || (!blogMenuButton.nextElementSibling.querySelector('li:first-of-type a').classList.contains('selected') && !blogMenuButton.nextElementSibling.querySelector('li:last-of-type a').classList.contains('selected'))) {
    blogMenuButton.onfocus = () => {
        if (!blogMenuButton.nextElementSibling.classList.contains('hide')) {
            hideSecondMenu();
        }
    }

    blogMenuButton.parentElement.previousElementSibling.firstElementChild.onfocus = () => {
        hideSecondMenu();
    }

    blogMenuButton.parentElement.nextElementSibling.firstElementChild.onfocus = () => {
        hideSecondMenu();
    }

    window.addEventListener('click', () => {
        hideSecondMenu();
    });
}

window.addEventListener('keyup', function(event) {
    if (event.keyCode == 27) {
        const secondMenuArray = Array.from(document.querySelectorAll('#blog-pages-list-button, #blog-select-page a'));
        if ((accessMode || window.innerWidth < 767 || window.innerHeight < 767)) {
            if (document.activeElement == blogMenuButton && blogMenuButton.nextElementSibling.classList.contains('hide')) {
                hideMenu();
                menuButton.focus();
            }
        }
        if (!blogMenuButton.nextElementSibling.classList.contains('hide') && secondMenuArray.some(elem => elem == document.activeElement)) {
            hideSecondMenu();
            blogMenuButton.focus();
        }
    }
});

window.addEventListener('resize', () => {
    hideSecondMenu();
    scrollCheck();
});

function hideSecondMenu() {
    blogMenuButton.setAttribute('aria-expanded', 'false');
    blogMenuButton.classList.remove('focus');
    blogMenuButton.nextElementSibling.classList.add('hide');
}

// check if scroll
function scrollCheck() {
    const footer = document.querySelector('footer.flex-line');
    const topBtn = document.getElementById('top-btn');
    getScrollableElem();
    if (document.body.classList.contains('texts') && !accessMode || document.body.classList.contains('posts') && !accessMode) {
        const size = document.querySelector("header nav").firstElementChild.offsetHeight;
        const innerContent = scrollableElem.querySelector("#list");
        if (window.innerWidth > 766 && window.innerHeight > 766) {
            footer.classList.remove('no-scroll');
            topBtn.parentElement.classList.remove('hide');
            scrollableElem.style.marginBottom = null;
            if (innerContent.offsetHeight <= scrollableElem.offsetHeight) {
                footer.classList.add('no-scroll');
                gif.classList.add('no-scroll')
                topBtn.parentElement.classList.add('hide');
                const mb = scrollableElem.offsetHeight - innerContent.offsetHeight;
                innerContent.style.marginBottom = Math.max(mb, 50) + 'px';
                document.querySelector('#main-container > aside.left > fieldset').classList.add('scrolled');
            }
        } 
        else {
            const offset = footer.classList.contains('no-scroll') ? 50 : 100;
            footer.classList.remove('no-scroll');
            topBtn.parentElement.classList.remove('hide');
            if (scrollableElem.offsetHeight > document.body.offsetHeight - offset) {
                footer.classList.add('no-scroll');
                topBtn.parentElement.classList.add('hide');
            } else {
                footer.classList.remove('no-scroll');
                topBtn.parentElement.classList.remove('hide');
            }
        }
        if (cssdisabled) {
            innerContent.style.marginBottom = 0;
        }
    }
}
