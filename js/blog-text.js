// var category = document.querySelector('.data').dataset.type;
// var categories = document.querySelector('aside.left ul');
// categories.classList.add('hide');

document.querySelector('#cross').addEventListener('click', function(event) {
    window.location.href = '../texts';
});

// enlargeImg();

function disableAsideRight() {
    if (window.innerWidth >= 767 && window.innerHeight >= 767 && !accessMode) {
        document.querySelector('#main-container > aside.right').setAttribute('aria-hidden', 'true');
        document.querySelectorAll('#main-container > aside.right > nav, #main-container > aside.right > nav > ul > li > a').forEach((elem) => {
            elem.setAttribute('tabindex', '-1');
        });
    }
}

function moveLink() {
    const link = document.querySelector('.source-link');

    if (window.innerWidth < 767 || window.innerHeight < 767 || accessMode) {
        if (link.parentElement.parentElement == document.querySelector('aside.right.plus')) {
            document.querySelector('article.two').append(link);
        }
    }
    else if (window.innerWidth >= 767 && window.innerHeight >= 767 && !accessMode) {
        if (link.parentElement == document.querySelector('article.two')) {
            document.querySelector('aside.right.plus div').append(link);
        }
    }
}

if (!cssdisabled) {
    disableAsideRight();
}
moveLink()

window.addEventListener('resize', () => {
    if (!cssdisabled) {
        disableAsideRight();
    }
    moveLink()
});

// handle media description (show/hide)
document.querySelectorAll('.media-container').forEach((elem) => {
    if (!elem.nextElementSibling || elem.nextElementSibling.nodeName !== 'FIGCAPTION') {
        elem.querySelectorAll('.media-button, .mediaDesc').forEach((el) => {
            el.classList.add('nocaption-bottom-margin');
        });
    }
});

function showMediaDesc() {
    const button = event.target.parentElement.querySelector('.media-button');
    button.nextElementSibling.classList.toggle('hide');
    button.classList.toggle('extra-bottom-margin');
    if (!button.parentElement.nextElementSibling || button.parentElement.nextElementSibling.nodeName !== 'FIGCAPTION') {
        button.classList.toggle('nocaption-bottom-margin');
    }
    let buttonValue = button.firstChild.textContent;
    const videoName = button.firstElementChild.textContent;
    if (button.nextElementSibling.classList.contains('hide')) {
        button.setAttribute('aria-expanded', 'false');
        if (document.firstElementChild.lang == 'en') {
            buttonValue = buttonValue.replace('Hide', 'Display');
        }
        else {
            buttonValue = buttonValue.replace('Cacher', 'Afficher');
        }
    }
    else {
        button.setAttribute('aria-expanded', 'true');
        if (document.firstElementChild.lang == 'en') {
            buttonValue = buttonValue.replace('Display', 'Hide');
        }
        else {
            buttonValue = buttonValue.replace('Afficher', 'Cacher');
        }
    }
    button.textContent = buttonValue;
    const videoSpan = document.createElement('span');
    videoSpan.classList.add('sr-only');
    videoSpan.textContent = videoName;
    button.append(videoSpan);
}

if (cssdisabled && accessMode) {
    document.querySelector('#top-accessible-banner li:last-of-type a').onclick = (e) => {
        e.preventDefault();
        document.querySelector('#bottom-accessible-banner li:first-of-type a').focus();
    }
}

// Fix footer bug on note click (hide footer to avoid html elem overflow)
window.addEventListener('load', () => {
    hideFooter();
});

window.addEventListener('hashchange', () => {
    hideFooter();
});

function hideFooter() {
    if (!accessMode && (window.innerWidth < 767 || window.innerHeight < 767)) {
        if (window.location.hash && window.location.hash !== 'start') {
            document.querySelector('footer').classList.add('hide');
            setTimeout(() => {
                document.querySelector('footer').classList.remove('hide');
            }, 100);
        }
    }
}
