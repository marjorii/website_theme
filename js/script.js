// check if css disabled
let testcss = document.createElement('div');
testcss.style.position = 'absolute';
document.getElementsByTagName('body')[0].appendChild(testcss);
if (testcss.currentStyle) currstyle = testcss.currentStyle['position'];
else if (window.getComputedStyle) currstyle = document.defaultView.getComputedStyle(testcss, null).getPropertyValue('position');
cssdisabled = (currstyle == 'static') ? true : false;
document.getElementsByTagName('body')[0].removeChild(testcss);

const accessMode = localStorage.getItem('accessible-mode') === 'true';
const accessModeBtn = document.getElementById('accessible-mode-btn');

if (document.title.includes('Page not Found')) {
    document.body.classList.add('error');
}

// gif
const gif = document.getElementById('dancing-banana');

document.querySelector('#pages li:nth-of-type(3) a').onmouseenter = function() {
    gif.classList.remove('invisible');
}
document.querySelector('#pages li:nth-of-type(3) a').onfocus = function() {
    gif.classList.remove('invisible');
}
document.querySelector('#pages li:nth-of-type(3) a').onmouseleave = function() {
    gif.classList.add('invisible');
}
document.querySelector('#pages li:nth-of-type(3) a').onblur = function() {
    gif.classList.add('invisible');
}

// check if tactile device
var isTactile = false;
if (/Android|webOS|iPhone|iPad|Kindle|Tablet|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    isTactile = true;
    gif.style.cssText = 'display: none';
}
else {
    isTactile = false;
}

let w = null
let h = null

// handle hash and scroll behavior
window.addEventListener("DOMContentLoaded", (event) => {
    if (window.location.hash !== '' && !document.body.classList.contains('projects') && !document.body.classList.contains('contact') && !window.location.href.includes('blog')) {
        const hash = String(window.location.hash.substring(1));
        if (hash == 'content' || hash == 'menu' || hash == 'aside') {
            let a11yBannerElems = Array.from(document.querySelectorAll('#top-accessible-banner a'));
            a11yBannerElems = a11yBannerElems.filter(hasHash);
            if (a11yBannerElems.some(elem => elem.hash.substring(1) == hash)) {
                anchorClick(a11yBannerElems.find(elem => elem.hash.substring(1) == hash));
            }
        }
        else if (hash == 'start' && !document.body.classList.contains('contact')) {
            window.scrollTo({ top: 0, behavior: 'auto' });
            setTimeout(function() {
                document.querySelector('h1').focus();
            }, 100);
        }
    }
    // deviceChange();

    setTimeout(scrollCheck, 300);
});

// document.onreadystatechange = function(e) {
//     if (document.readyState === 'complete') {
//         //dom is ready, window.onload fires later
//         deviceChange();
//     }
// }

// mobile menu
const menuButton = document.getElementById('menu');
const menu = document.getElementById('pages');

menuButton.addEventListener("click", function(e) {
    const firstListElem = document.querySelector('#list > ul > li:first-of-type > div');
    if (menu.classList.contains('hide')) {
        this.setAttribute('aria-expanded', 'true');
        if (document.body.classList.contains('projects')) {
            if (!accessMode && window.innerWidth < 314 && (firstListElem && firstListElem.classList.contains('focus'))) {
                this.classList.remove('removeStroke');
                this.classList.remove('addStroke');
            }
        }
    }
    else {
        this.setAttribute('aria-expanded', 'false');
        if (document.body.classList.contains('projects') && (firstListElem && firstListElem.classList.contains('focus'))) {
            // if (!accessMode && window.innerWidth < 314) {
            //     this.classList.add('removeStroke');
            // }
        }
        if (window.location.href.includes('blog') && !document.body.classList.contains('error')) {
            const blogMenuButton = document.getElementById('blog-pages-list-button');
            if (!accessMode || (window.innerWidth >= 767 && window.innerHeight >= 767)) {
                if (!blogMenuButton.nextElementSibling.querySelector('li:first-of-type a').classList.contains('selected') || blogMenuButton.nextElementSibling.querySelector('li:last-of-type a').classList.contains('selected')) {
                    blogMenuButton.setAttribute('aria-expanded', 'false');
                    blogMenuButton.classList.remove('focus');
                    blogMenuButton.nextElementSibling.classList.add('hide');
                }
                else {
                    blogMenuButton.setAttribute('aria-expanded', 'true');
                    blogMenuButton.classList.add('focus');
                    blogMenuButton.nextElementSibling.classList.remove('hide');
                }
            }
        }
    }
    menu.classList.toggle('hide');
    menuButton.parentElement.classList.toggle('extra-border');
    e.preventDefault();
    e.stopPropagation();
})

// close mobile menu on some elems focus
menuButton.onfocus = (e) => {
    if (!menu.classList.contains('hide')) {
        setTimeout(() => {
            if (!menu.classList.contains('hide')) {
                hideMenu();
            }
        }, 100)
    }
}

if (accessMode || window.innerWidth < 767 || window.innerHeight < 767) {
    let lastButton = undefined;
    if (window.location.href.includes('blog')) {
        lastButton = document.querySelector('#top-accessible-banner li:not(.desktop) a')
    }
    else {
        lastButton = document.querySelector('#top-accessible-banner li:last-of-type a')
    }
    lastButton.onfocus = () => {
        if (!menu.classList.contains('hide')) {
            hideMenu();
        }
    }
}

if (accessMode || window.innerWidth < 767 || window.innerHeight < 767) {
    setTimeout(() => {
        menu.lastElementChild.firstElementChild.onblur = (e) => {
            if (e.relatedTarget != menu.lastElementChild.previousElementSibling.firstElementChild) {
                hideMenu();
            }
        }
    }, 100);
}

// close mobile menu on window click
window.addEventListener('click', function(e) {
    if (window.innerWidth < 767 || window.innerHeight < 767 || accessMode) {
        hideMenu();
    }
});

// close mobile menu on ESC key press and remove gif if visible
window.addEventListener('keyup', function(event) {
    if (event.keyCode == 27) {
        if (accessMode || window.innerWidth < 767 || window.innerHeight < 767) {
            const menuArray = Array.from(document.querySelectorAll('#menu, #pages > li > a'));
            if (!menu.classList.contains('hide') && menuArray.some(elem => elem == document.activeElement)) {
                    hideMenu();
                    menuButton.focus();
            }
        }
        else {
            if (!gif.classList.contains('invisible')) {
                gif.classList.add('invisible');
            }
        }
    }
});

// handle basic features (desktop/mobile switch, a11y banners)

// handle desktop/mobile menu
deviceChange();
// move aside menu from menu to aside right
moveAsideMenu();
if (!document.body.classList.contains('contact') && !accessMode) {
    // overwrite anchor behaviour for quick access elems in top-accessible-banner
    scrollToAnchor();
}
if (!accessMode) {
    // stick menu on scroll
    menuStick();
    // show back to top link if defilable div entirely scrolled
    accessFooter();
}
// add tabindex on txt elems if projects page and desktop
tabCheck();


window.addEventListener('resize', () => {
    // if (h !== null && window.innerHeight > h) {
    //     document.querySelector("body > footer").style.height = `calc(100vh - ${50 - (h - window.innerHeight)})`
    // }
    if ((window.innerWidth < 767 || window.innerHeight < 767) && isTactile) {
        if (w !== null && window.innerWidth !== w) {
            w = window.innerWidth;
            h = window.innerHeight;
        }
        else {
            // e.preventDefault();
            return
        }
    }

    deviceChange();
    moveAsideMenu();
    if (!document.body.classList.contains('contact') && !accessMode) {
        scrollToAnchor();
    }
    if (!accessMode) {
        menuStick();
        accessFooter();
    }

    window.scrollTo({top: 0, behavior: 'auto' });
    document.querySelectorAll('header.banner, main, header > nav > div, aside, #main-container, #pages, footer, #dancing-banana').forEach((elem) => {
        elem.classList.remove('scrolled');
    });
    if (document.body.classList.contains('text') || document.body.classList.contains('posts') && !window.location.pathname.indexOf('/blog/publications/posts/') > -1) {
        if (document.getElementById('cross')) {
            document.getElementById('cross').classList.remove('scrolled');
        }
    }
    scrollCheck();

    const topBtn = document.getElementById('top-btn');
    getScrollableElem();
    if (document.body.classList.contains('legal-notices') && (window.innerWidth < 767 || window.innerHeight < 767) && !accessMode) {
        if (scrollableElem.offsetHeight == scrollableElem.scrollHeight) {
            document.getElementById('footer-nav').style.bottom = -61 + 'px';
            topBtn.parentElement.classList.add('hide');
        }
    }

    tabCheck();

});

window.addEventListener('load', () => {
    const topBtn = document.getElementById('top-btn');
    getScrollableElem();
    if (document.body.classList.contains('legal-notices') && (window.innerWidth < 767 || window.innerHeight < 767) && !accessMode) {
        if (scrollableElem.offsetHeight == scrollableElem.scrollHeight) {
            document.getElementById('footer-nav').style.bottom = -61 + 'px';
            topBtn.parentElement.classList.add('hide');
        }
    }
});

if (cssdisabled) {
    gif.style.cssText = 'display: none';
}

// accessible mode
accessModeBtn.onclick = () => {
    const inverted = localStorage.getItem('accessible-mode') === "true" ? false : true
    localStorage.setItem('accessible-mode', inverted)
    // reload page to reset functionnalities
    location.reload();
}

if (accessMode) {
    switchMode(true);
    accessModeBtn.setAttribute('aria-pressed', 'true');
}
else {
    accessModeBtn.setAttribute('aria-pressed', 'false');
}

// back to top button
if (!document.body.classList.contains('contact')) {
    const topBtn = document.getElementById('top-btn');
    getScrollableElem();
    topBtn.onclick = (e) => {
        e.preventDefault();
        scrollableElem.scrollTo({ top: 0, behavior: 'smooth' });
        setTimeout(function() {
            location.hash = topBtn.hash;
            document.querySelector('h1').focus();
            // Do not double scroll to top if page has long content because of scroll animation being longer than the timeout value (100ms)
            if (!document.body.classList.contains('about') && !document.body.classList.contains('resources') && !document.body.classList.contains('text') || accessMode) {
                scrollableElem.scrollTo({top: 0, behavior: 'auto'});
            }
            else if (window.innerWidth < 314 && !accessMode && (document.body.classList.contains('about') || document.body.classList.contains('resources') || document.body.classList.contains('text'))) {
                scrollableElem.scrollTo({top: 0, behavior: 'auto'});
            }
        }, 100);
    }
}

// UTILS

function getScrollableElem() {
    const main = document.querySelector('main');
    const html = document.querySelector('html');
    let scrollableElem = undefined;
    if (window.innerWidth >= 767 && window.innerHeight >= 767 || accessMode) {
        scrollableElem = main;
    }
    else if (window.innerWidth < 767 || window.innerHeight < 767 || accessMode) {
        menu.classList.remove('flex-line');
        menu.classList.add('select-page', 'hide');
        if (!accessMode && (document.body.classList.contains('about')) || !accessMode && (document.body.classList.contains('resources')) || !accessMode && (!document.body.classList.contains('text')) || !accessMode && (document.body.classList.contains('sitemap')) || !accessMode && (document.body.classList.contains('accessibility')) || !accessMode && (document.body.classList.contains('legal-notices'))) {
            scrollableElem = main;
        }
        else {
            scrollableElem = html;
        }
    }
    return scrollableElem;
}

function deviceChange() {
    const main = document.querySelector('main');
    const html = document.querySelector('html');
    getScrollableElem();
    if (window.innerWidth >= 767 && window.innerHeight >= 767 && !accessMode) {
        menu.classList.replace('select-page', 'flex-line');
        if (menu.classList.contains('hide')) {
            menu.classList.remove('hide');
        }
        scrollableElem = main;
    }
    else if (window.innerWidth < 767 || window.innerHeight < 767 || accessMode) {
        menu.classList.replace('flex-line', 'select-page');
        hideMenu();
        if (!accessMode && (document.body.classList.contains('about') || document.body.classList.contains('resources') || document.body.classList.contains('text') || document.body.classList.contains('sitemap') || document.body.classList.contains('accessibility') || document.body.classList.contains('legal-notices'))) {
            if (window.innerWidth < 314) {
                scrollableElem = html;
            }
            else {
                scrollableElem = main;
            }
        }
        else {
            scrollableElem = html;
        }
    }
    return scrollableElem;
}

// activate/disable accessible mode
function switchMode(force) {
    const darkmode = localStorage.getItem('accessible-mode') === 'true';
    localStorage.setItem('accessible-mode', force || !darkmode)

    if (force || !darkmode) {
        document.body.classList.add('accessible-mode');
    }
    else {
        document.body.classList.remove('accessible-mode');
    }
}

function hideMenu() {
    const firstListElem = document.querySelector('#list > ul > li:first-of-type > div');
    if (!menu.classList.contains('hide')) {
        menu.classList.add('hide');
        menuButton.setAttribute('aria-expanded', 'false');
        menuButton.parentElement.classList.remove('extra-border');
        if (!accessMode && window.innerWidth < 314 && (firstListElem && firstListElem.classList.contains('focus'))) {
            menuButton.classList.add('removeStroke');
        }
    }
}

function menuStick() {
    if (window.innerWidth > 320) {
        getScrollableElem();
        if (window.innerWidth >= 767 && window.innerHeight >= 767  && !accessMode || document.body.classList.contains('about') || document.body.classList.contains('resources') || document.body.classList.contains('text') || document.body.classList.contains('sitemap') || document.body.classList.contains('legal-notices') || document.body.classList.contains('accessibility')) {
            scrollableElem.addEventListener('scroll', onScroll);
        }
        else if (window.innerWidth < 767 || window.innerHeight < 767  || accessMode) {
            window.addEventListener('scroll', onScroll);
        }
    }
}

function onScroll() {
    const asideScrollingElems = document.querySelectorAll('header.banner, main, header > nav > div, aside');
    const mainScrollingElems = document.querySelectorAll('#main-container, #pages')
    getScrollableElem();
    if (scrollableElem.scrollTop > 0) {
        if (document.body.classList.contains('legal-notices')) {
            if ((scrollableElem.scrollHeight - scrollableElem.offsetHeight) > document.getElementById('top-accessible-banner').offsetHeight) {
                asideScrollingElems.forEach((elem) => {
                    elem.classList.add('scrolled');
                });
            }
        }
        else {
            asideScrollingElems.forEach((elem) => {
                    elem.classList.add('scrolled');
            });
        }
        if (window.innerWidth < 767 || window.innerHeight < 767) {
            mainScrollingElems.forEach((elem) => {
                if (document.body.classList.contains('legal-notices')) {
                    if ((scrollableElem.scrollHeight - scrollableElem.offsetHeight) > document.getElementById('top-accessible-banner').offsetHeight) {
                        elem.classList.add('scrolled');
                    }
                }
                else {
                    elem.classList.add('scrolled');
                }
            });
        }
    }
    else {
        asideScrollingElems.forEach((elem) => {
            elem.classList.remove('scrolled');
        });
        if (window.innerWidth < 767 || window.innerHeight < 767) {
            mainScrollingElems.forEach((elem) => {
                elem.classList.remove('scrolled');
            });
        }
    }
    // handle scroll on focus (if focused elem is outside of window)
    if (window.innerWidth >= 767 && window.innerHeight >= 767 && !document.body.classList.contains('contact') && !accessMode) {
        if (document.firstElementChild.scrollTop > 0 && !document.body.classList.contains('scrolled')) {
            document.body.classList.add('scrolled');
            document.body.style.top = document.firstElementChild.scrollTop + 'px';
        }
        else {
            document.body.classList.remove('scrolled');
            document.body.style.top = 0;
            window.scrollTo({top: 0, behavior: 'auto' });
        }
    }
}

function scrollToAnchor() {
    const a11yBannerElems = Array.from(document.querySelectorAll('#top-accessible-banner li *'));
    const filteredElems = a11yBannerElems.filter(hasHash);
    getScrollableElem();
    filteredElems.forEach((element) => {
        element.onclick = function(e) {
            e.preventDefault();
            anchorClick(element)
        }
    });

    // On top or bottom banner buttons focus, scrollTo banner
    a11yBannerElems.forEach((elem) => {
        elem.addEventListener('focus', (e) => {
            if (scrollableElem.scrollTop != 0) {
                window.scrollTo({top: 0, behavior: 'auto' });
                document.querySelectorAll('header.banner, main, header > nav > div, aside, #main-container, #pages').forEach((el) => {
                    el.classList.remove('scrolled');
                });
                if (document.body.classList.contains('text') || document.body.classList.contains('posts') && !window.location.pathname.indexOf('/blog/publications/posts/') > -1) {
                    if (document.getElementById('cross')) {
                        document.getElementById('cross').classList.remove('scrolled');
                    }
                }
            }
            if (document.body.classList.contains('scrolled')) {
                document.body.classList.remove('scrolled');
                document.body.style.top = 0;
                scrollableElem.scrollTo({top: 0, behavior: 'auto' });
            }
        });
    });
    document.querySelectorAll('#bottom-accessible-banner a').forEach((elem) => {
        elem.addEventListener('focus', (e) => {
            if (document.body.classList.contains('posts') || document.body.classList.contains('texts')) {
                // if top-btn is not visible, scroll to bottom
                if (document.querySelector("#top-btn").getBoundingClientRect().top > window.innerHeight) {
                    scrollableElem.scrollTo({top: scrollableElem.scrollHeight, behavior: 'auto' });
                }
            }
            else {
                if ((scrollableElem.scrollTop + window.innerHeight) != scrollableElem.scrollHeight) {
                    scrollableElem.scrollTo({top: scrollableElem.scrollHeight, behavior: 'auto' });
                }
            }
        });
    });

}

function hasHash(elem) {
    return elem.hash;
}

function anchorClick(elem) {
    const elemId = String(elem.hash.substring(1));
    scrollableElem.scrollTo({top: 5, behavior: 'auto' });

    location.hash = elemId;

    if (window.innerWidth >= 767 && window.innerHeight >= 767 || document.body.classList.contains('about') || document.body.classList.contains('resources') || document.body.classList.contains('text')) {
        window.scrollTo({top: 0, behavior: 'auto'});
    }

    document.getElementById(elemId).focus();

    if (window.innerWidth < 767 || window.innerHeight < 767) {
        scrollableElem.scrollTo({top: 1, behavior: 'auto'});
        // if (document.body.classList.contains('sitemap')) {
        //     if (!accessMode && window.innerWidth > 313) {
        //         scrollableElem.scrollTo({top: -10, behavior: 'auto'});
        //     }
        // }
    }

}

function moveAsideMenu() {
    let asideMenu = Array.from(document.querySelectorAll('.aside-menu'));
    let asideUl = document.querySelector('aside.right ul');
    if (window.innerWidth >= 767 && window.innerHeight >= 767 && !accessMode) {
        if (asideMenu[0].parentElement == menu) {
            asideMenu.forEach((li) => {
                asideUl.append(li);
            });
        }
    }
    else if (window.innerWidth < 767 || window.innerHeight < 767 || accessMode) {
        if (asideMenu[0].parentElement == asideUl) {
            asideMenu.forEach((li) => {
                menu.append(li);
            });
        }
    }
}

function accessFooter() {
    const lastListElem = document.querySelector('#list > ul > li:last-of-type');
    const scrollingElems = document.querySelectorAll('#main-container > aside.left > fieldset, footer, #dancing-banana');
    const footerNav = document.getElementById('footer-nav');
    getScrollableElem();
    if (window.innerWidth >= 767 && window.innerHeight >= 767) {
        scrollableElem.addEventListener('scroll', () => {
            if ((scrollableElem.offsetHeight + scrollableElem.scrollTop) >= scrollableElem.scrollHeight) {
                if (document.body.classList.contains('projects')) {
                    lastListElem.classList.add('scrolled');
                }
                scrollingElems.forEach((elem) => {
                    elem.classList.add('scrolled');
                });
            }
            else {
                if (document.body.classList.contains('projects')) {
                    lastListElem.classList.add('scrolled');
                }
                scrollingElems.forEach((elem) => {
                    if (!document.body.classList.contains('resources') && document.querySelector('main > div').style.marginBottom === '') {
                        elem.classList.remove('scrolled');
                    }
                    else if (document.body.classList.contains('resources') && document.querySelector('main section').style.marginBottom === '') {
                        elem.classList.remove('scrolled');
                    }
                    else if (document.body.classList.contains('legal-notices') && scrollableElem.scrollHeight > scrollableElem.offsetHeight) {
                        elem.classList.remove('scrolled');
                    }
                });
            }
        });
    }
    else {
        if (document.body.classList.contains('about') || document.body.classList.contains('resources') || document.body.classList.contains('text') || document.body.classList.contains('sitemap') || document.body.classList.contains('accessibility') || document.body.classList.contains('legal-notices')) {
            scrollableElem.addEventListener('scroll', () => {
                if ((scrollableElem.offsetHeight + scrollableElem.scrollTop) >= scrollableElem.scrollHeight) {
                    footerNav.classList.add('scrolled');
                    if (document.body.classList.contains('legal-notices')) {
                        if (document.querySelector('footer.flex-line').style.bottom) {
                            footerNav.classList.remove('scrolled');
                        }
                    }
                }
                else {
                    footerNav.classList.remove('scrolled');
                }
            });
        }
    }

    if (document.body.classList.contains('text') || document.body.classList.contains('posts') && !window.location.pathname.indexOf('/blog/publications/posts/') > -1) {
        if (document.getElementById('cross')) {
            const cross = document.getElementById('cross');
            if ((window.innerWidth >= 767 && window.innerHeight >= 767) || document.body.classList.contains('text') && window.innerWidth >= 314) {
                scrollableElem.addEventListener('scroll', () => {
                    if (scrollableElem.scrollTop > 0) {
                        cross.classList.add('scrolled');
                    }
                    else {
                        cross.classList.remove('scrolled');
                    }
                });
            }
            else {
                window.addEventListener('scroll', () => {
                    if (scrollableElem.scrollTop > 0) {
                        cross.classList.add('scrolled');
                    }
                    else {
                        cross.classList.remove('scrolled');
                    }
                });
            }
        }
    }
}

// enlarge img on click
// function enlargeImg() {
//     const img = document.querySelectorAll('main img');
//     for(let i = 0; i < img.length; i++) {
//         img[i].addEventListener("click", function() {
//             window.location = img[i].src;
//         });
//     }
// }

// check if scroll
function scrollCheck() {
    const footer = document.querySelector('footer.flex-line');
    const topBtn = document.getElementById('top-btn');
    getScrollableElem();
    if (document.body.classList.contains('legal-notices') && !accessMode) {
        const size = document.querySelector("header nav").firstElementChild.offsetHeight
        const innerContent = scrollableElem.querySelector("div")
        if (window.innerWidth > 766 && window.innerHeight > 766) {
            footer.classList.remove('no-scroll');
            topBtn.parentElement.classList.remove('hide');
            scrollableElem.style.marginBottom = null
            if (innerContent.offsetHeight <= scrollableElem.offsetHeight) {
                footer.classList.add('no-scroll');
                topBtn.parentElement.classList.add('hide');
                const mb = scrollableElem.offsetHeight - innerContent.offsetHeight
                innerContent.style.marginBottom = Math.max(mb, 61) + 'px'
            }
        }
        else {
            footer.classList.remove('no-scroll');
            topBtn.parentElement.classList.remove('hide');
            // if (innerContent.offsetHeight <= scrollableElem.offsetHeight) {
            if (innerContent.offsetHeight <= scrollableElem.offsetHeight && window.innerWidth > 313) {
                footer.classList.add('no-scroll');
                topBtn.parentElement.classList.add('hide');
                footer.style.bottom = '0';
                footer.style.bottom = '61px';
            }
        }
        if (cssdisabled) {
            innerContent.style.marginBottom = 0;
        }
    }
}

function tabCheck() {
    if (!accessMode) {
        if (document.body.classList.contains('projects')) {
            document.querySelectorAll('.txt, .infos').forEach((txt) => {
                if (window.innerWidth > 1199) {
                    if (!txt.getAttribute('tabindex')) { 
                        txt.setAttribute('tabindex', '0');
                    }
                }
                else {
                    if (txt.getAttribute('tabindex')) { 
                        txt.removeAttribute('tabindex');
                    }
                }
            });
        }
    }
    else {
        document.querySelectorAll('.txt, .infos').forEach((txt) => {
            if (txt.getAttribute('tabindex')) { 
                txt.removeAttribute('tabindex');
            }
        });
    }
}
