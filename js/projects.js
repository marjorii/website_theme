// FIXME /!\ nav issue when navigate with up or down arrow keys? : when project div has focus, only time is vocalized... + sometimes seems to fuck with scroll...
// works with Orca if button out of h2 but not if button is inside h2..., test with NVDA to see if problem persists

var activeLink;
const modal = document.querySelector('#project-container');
if (cssdisabled) {
    modal.remove();
}

// handle hash and scroll behavior
window.addEventListener("DOMContentLoaded", (event) => {
    if (window.location.hash !== '') {
        const hash = String(window.location.hash.substring(1));
        if (hash == 'content' || hash == 'menu' || hash == 'aside') {
            let a11yBannerElems = Array.from(document.querySelectorAll('#top-accessible-banner a'));
            a11yBannerElems = a11yBannerElems.filter(hasHash);
            if (a11yBannerElems.some(elem => elem.hash.substring(1) == hash)) {
                anchorClick(a11yBannerElems.find(elem => elem.hash.substring(1) == hash));
            }
        }
        else if (hash == 'start') {
            window.scrollTo({ top: 0, behavior: 'auto' });
            setTimeout(function() {
                document.querySelector('h1').focus();
            }, 100);
        }
        else {
            if (!cssdisabled) {
                document.querySelector(window.location.hash).click();

                if ((window.innerWidth < 767 || window.innerHeight < 767) && !accessMode) {
                    if (document.getElementById(hash).parentElement.parentElement.parentElement.parentElement == document.querySelectorAll('#list li')[0]) {
                        scrollableElem.scrollTo({top: 2, behavior: 'auto' });
                    }
                    else {
                        if (window.innerWidth < window.innerHeight) {
                            scrollableElem.scrollTo({top: document.getElementById(hash).offsetTop - 70, behavior: 'auto' });
                        }
                        else {
                            scrollableElem.scrollTo({top: document.getElementById(hash).offsetTop - 120, behavior: 'auto' });
                        }
                    }
                }
            }
        }
    }
    // moveProjectsDetails()
});

// document.onreadystatechange = function(e) {
//     if (document.readyState === 'complete') {
//         moveProjectsDetails()
//     }
// }

// gallery: prev + next project
document.querySelectorAll('#prev, #next').forEach(function(changeElem) {
    changeElem.addEventListener('click', function changeProject(event) {
        let projects = document.querySelectorAll('.item');
        let index = Array.prototype.indexOf.call(projects, activeLink);
        let direction = changeElem.id == 'prev' ? -1 : 1;
        let nextPrevProject = projects[index + direction];

        if (nextPrevProject == undefined) {
            if (direction == -1) {
                nextPrevProject = projects[projects.length -1];
            }
            else {
                nextPrevProject = projects[0];
            }
        }
        const galleryBtn = nextPrevProject.parentElement.parentElement.parentElement.parentElement.querySelector('.gallery');
        onClose();
        galleryBtn.click();
    });
});

// open project on .project div click (buttons = pointer-events: none)
document.querySelectorAll('.project').forEach(function(elem) {
    elem.addEventListener('click', function(event) {
        const content = elem.nextElementSibling;
        openProject(elem.querySelector('button'), content);
    });
});

document.querySelectorAll('.item').forEach(function(elem) {

    const proj = elem.parentElement.parentElement.parentElement;
    const galleryButton = elem.parentElement.parentElement.parentElement.parentElement.querySelector('.gallery');
    const accessibleGallery = galleryButton.nextElementSibling;
    const main = document.querySelector('main');

    if (cssdisabled) {
        galleryButton.remove();
    }

    if (!accessMode) {
        if (!cssdisabled) {
            elem.addEventListener('focus', () => {
                if (document.firstElementChild.scrollTop > 0 && !document.body.classList.contains('scrolled') && window.innerWidth >= 767 && window.innerHeight >= 767) {
                    document.body.classList.add('scrolled');
                    document.body.style.top = document.firstElementChild.scrollTop + 'px';
                }
            });
        }
    }
    else {
        if (elem.parentElement.parentElement.parentElement.classList.contains('plusplusegal')) {
            elem.removeAttribute('aria-label');
        }
    }

    // elem.addEventListener('focus', () => {
    //     proj.classList.add('focus');
    // });
    //
    // elem.addEventListener('blur', () => {
    //     proj.classList.remove('focus');
    // });

    if (!cssdisabled) {
        if (accessMode) {
            galleryButton.setAttribute('aria-expanded', 'false');
            galleryButton.setAttribute('aria-controls', galleryButton.nextElementSibling.id);
        }
        else {
            galleryButton.removeAttribute('aria-expanded');
            galleryButton.removeAttribute('aria-controls');
        }
    }

    // FIXME /!\ menu pages visible on desktop sometimes (appears under prev/next header...)
    // happens on gallery button click when page's scrolled and header's sticking top of window
    // does it have something to do with 'scrolled' class on body?

    // on Images gallery click
    if (!cssdisabled) {
        galleryButton.addEventListener('click', function(event) {

            if (!accessMode) {
                const cross = document.getElementById('cross');
                event.preventDefault();
                activeLink = elem;

                document.querySelectorAll('aside.menu-bar.right > *').forEach((el) => {
                    el.classList.add('hide');
                });

                fetch(elem.dataset.url + ".json")
                .then(function(response) {
                    return response.json();
                })
                .then(function(json) {
                    document.querySelector('.project-gallery').innerHTML = json.content;
                    main.classList.add('show-project');
                    document.body.classList.add('gallery-mode');
                    document.getElementById('dancing-banana').style.opacity = 0;

                    tabSelectors = '#top-banner-accessible a, #top-banner-accessible button, #bottom-accessible-banner a, #pages a, header.website li > *, .item, .gallery, .links a, #main-container > aside.right > ul > li > *';
                    document.querySelectorAll(tabSelectors).forEach(function(element) {
                        element.setAttribute('tabindex', '-1');
                    });

                    var category = elem.parentElement.parentElement.parentElement.dataset.type;
                    var categories = document.querySelector('aside.left fieldset');
                    if (main.classList.contains('show-project')) {
                        if (elem.id.includes('super-image')) {
                            document.querySelector('#img > img:first-of-type').classList.add('portrait');
                        }

                        categories.classList.add('hide');

                        // enlargeImg();
                    }

                    document.querySelector('footer p').innerHTML = json.footer;

                    main.classList.add('no-scroll');
                    modal.scrollTop = 0;
                    modal.dataset.project = elem.id;
                    location.hash = elem.id;

                    // modal specific

                    modal.nextElementSibling.classList.remove('hide');

                    // focus first focusable elem
                    cross.focus();
                    // trap focus
                    document.addEventListener('focus', trapFocus, true);
                    // no click out of modal
                    document.addEventListener('click', noClick, true);

                    // if back tab on modal focus, focus last focusable element
                    document.addEventListener('keyup', function(event) {
                        if (document.activeElement === modal) {
                            if (event.code === 'Tab') {
                                cross.focus();
                            }
                            if (event.shiftKey && event.code === 'Tab') {
                                document.getElementById('next').focus();
                            }
                            event.stopPropagation();
                        }
                    }, false);

                    //

                    mobileHeader();
                    window.addEventListener('resize', () => {
                        mobileHeader();
                    });

                    document.querySelectorAll('.item').forEach((item) => {
                        if (item.parentElement.parentElement.parentElement.classList.contains('focus')) {
                            item.parentElement.parentElement.parentElement.classList.remove('focus');
                        }
                    });

                }).then(function() {
                    window.scrollTo({top: 0, behavior: 'auto' });
                });

            }
            // if accessMode
            else {
                accessibleGallery.classList.toggle('hide');
                if (accessibleGallery.classList.contains('hide')) {
                    galleryButton.setAttribute('aria-expanded', 'false');
                }
                else {
                    galleryButton.setAttribute('aria-expanded', 'true');
                }
            }

        }, false);
    }
});

// img tooltip + mouse follow on project mouseover
// document.querySelectorAll('#list li > span').forEach(function(elem) {
//     elem.onmousemove = function(e) {
//         var x = (e.pageX - 100) + 'px',
//             y = (e.pageY - 100) + 'px';
//         elem.querySelector('img').style.top = y;
//         elem.querySelector('img').style.left = x;
//     }
// });

// on project opening
function openProject(elem, content) {
    content.classList.toggle('hide');
    if (content.classList.contains('hide')) {
        elem.setAttribute('aria-expanded', 'false');
        location.hash = '';
        elem.parentElement.parentElement.parentElement.classList.remove('focus');
        if (!accessMode && menuButton.classList.contains('removeStroke')) {
            menuButton.classList.remove('removeStroke');
        }
    }
    else {
        elem.setAttribute('aria-expanded', 'true');
        modal.dataset.project = elem.id;
        location.hash = elem.id;
        elem.parentElement.parentElement.parentElement.classList.add('focus');

        if (!accessMode && !cssdisabled) {
            window.scrollTo({top: 0, behavior: 'auto' });
        }

        if (!accessMode && window.innerWidth < 314 && elem.parentElement.parentElement.parentElement.parentElement == elem.parentElement.parentElement.parentElement.parentElement.parentElement.firstElementChild && elem.parentElement.parentElement.parentElement.classList.contains('focus')) {
            menuButton.classList.add('removeStroke');
        }

        // handle project scroll into view on mobile
        if (!cssdisabled) {
            if ((window.innerWidth < 767 || window.innerHeight < 767) && !accessMode) {
                if (elem.parentElement.parentElement.parentElement.parentElement == document.querySelectorAll('#list li')[0]) {
                    if (window.innerWidth < 314) {
                        scrollableElem.scrollTo({top: elem.offsetTop + 50, behavior: 'auto' });
                    }
                    else if (window.innerWidth >= 542 && window.innerWidth < 600) {
                        scrollableElem.scrollTo({top: 48, behavior: 'auto' });
                    }
                    else if (window.innerWidth >= 600) {
                        // scrollableElem.scrollTo({top: 1, behavior: 'auto' });
                        scrollableElem.scrollTo({top: 51, behavior: 'auto' });
                    }
                    if (document.firstElementChild.lang == 'en') {
                        if (window.innerWidth >= 314 && window.innerWidth < 542) {
                            scrollableElem.scrollTo({top: 54, behavior: 'auto' });
                        }
                    }
                    else {
                        if (window.innerWidth >= 314 && window.innerWidth < 320) {
                            scrollableElem.scrollTo({top: 102, behavior: 'auto' });
                        }
                        else if (window.innerWidth >= 320 && window.innerWidth < 542) {
                            scrollableElem.scrollTo({top: 78, behavior: 'auto' });
                        }
                    }
                }
                else {
                    if (!document.getElementById('main-container').classList.contains('scrolled')) {
                        if (window.innerWidth < window.innerHeight) {
                            if (document.firstElementChild.lang == 'en') {
                                if (window.innerWidth < 314) {
                                    scrollableElem.scrollTo({top: elem.offsetTop + 50, behavior: 'auto' });
                                }
                                else if (window.innerWidth >= 314 && window.innerWidth < 321) {
                                    scrollableElem.scrollTo({top: elem.offsetTop - 147, behavior: 'auto' });
                                }
                                else if (window.innerWidth >= 321 && window.innerWidth < 350) {
                                    scrollableElem.scrollTo({top: elem.offsetTop - 123, behavior: 'auto' });
                                }
                                else if (window.innerWidth >= 350 && window.innerWidth < 550) {
                                    scrollableElem.scrollTo({top: elem.offsetTop - 86, behavior: 'auto' });
                                }
                                else if (window.innerWidth >= 550) {
                                    scrollableElem.scrollTo({top: elem.offsetTop - 62, behavior: 'auto' });
                                }
                            }
                            else {
                                if (window.innerWidth < 314) {
                                    scrollableElem.scrollTo({top: elem.offsetTop + 50, behavior: 'auto' });
                                }
                                else if (window.innerWidth >= 314 && window.innerWidth < 367) {
                                    scrollableElem.scrollTo({top: elem.offsetTop - 99, behavior: 'auto' });
                                }
                                else if (window.innerWidth >= 367) {
                                    scrollableElem.scrollTo({top: elem.offsetTop - 62, behavior: 'auto' });
                                }
                            }
                        }
                        else {
                            if (window.innerWidth < 767) {
                                scrollableElem.scrollTo({top: elem.offsetTop - 58, behavior: 'auto' });
                            }
                            else {
                                scrollableElem.scrollTo({top: elem.offsetTop - 120, behavior: 'auto' });
                            }
                        }
                    }
                    else {
                        if (window.innerWidth < window.innerHeight) {
                            if (window.innerWidth < 314) {
                                scrollableElem.scrollTo({top: elem.offsetTop + 215, behavior: 'auto' });
                            }
                            else if (window.innerWidth > 314 && window.innerWidth < 600) {
                                scrollableElem.scrollTo({top: elem.offsetTop + 45, behavior: 'auto' });
                            }
                            else if (window.innerWidth > 599) {
                                scrollableElem.scrollTo({top: elem.offsetTop + 50, behavior: 'auto' });
                            }
                        }
                        else {
                            if (window.innerWidth < 767) {
                                scrollableElem.scrollTo({top: elem.offsetTop + 50, behavior: 'auto' });
                            }
                            else {
                                scrollableElem.scrollTo({top: elem.offsetTop, behavior: 'auto' });
                            }
                        }
                    }
                }
            }
        }
    }
}

// on images gallery closure
function onClose() {
    const main = document.querySelector('main');
    const projHeader = document.querySelector('.projects header');
    document.body.classList.remove('no-scroll');
    main.classList.remove('no-scroll');
    main.classList.remove('show-project');
    document.body.classList.remove('gallery-mode');
    if (projHeader.classList.contains('blur')) {
        projHeader.classList.remove('blur');
    }
    if (projHeader.classList.contains('index-change')) {
        projHeader.classList.remove('index-change');
    }
    if (projHeader.firstElementChild.classList.contains('hide')) {
        projHeader.firstElementChild.classList.remove('hide')
    }
    document.querySelectorAll('video').forEach(function(vid){
        vid.pause();
        vid.currentTime = 0;
    });

    tabSelectors = '#top-banner-accessible a, #top-banner-accessible button, #bottom-accessible-banner a, #pages a, header.website li > *, .item, .gallery, .links a, #main-container > aside.right > ul > li > *';
    document.querySelectorAll(tabSelectors).forEach(function(element) {
        element.removeAttribute('tabindex');
    });

    document.querySelector('aside.left fieldset').classList.remove('hide');

    // activeLink.focus();

    // handle project scroll into view on mobile
    if (!cssdisabled) {
        if (window.innerWidth < 767 || window.innerHeight < 767) {
            if (window.innerWidth < window.innerHeight) {
                if (window.innerWidth < 314) {
                    scrollableElem.scrollTo({top: activeLink.offsetTop + 50, behavior: 'auto' });
                }
                else {
                    scrollableElem.scrollTo({top: activeLink.offsetTop - 70, behavior: 'auto' });
                }
            }
        }
    }

    document.querySelectorAll('.content:not(.hide)').forEach((elem) => {
        elem.previousElementSibling.classList.add('focus');
    });

    document.querySelectorAll('aside.menu-bar.right > *').forEach((el) => {
        el.classList.remove('hide');
    });

    activeLink.parentElement.parentElement.parentElement.classList.add('focus');

    removeListeners();

    modal.nextElementSibling.classList.add('hide');
}

function mobileHeader() {
    const main = document.querySelector('main');
    const projHeader = document.querySelector('.projects header');
    if (main.classList.contains('show-project')) {
        if (window.innerWidth <= 1000 || window.innerHeight <= 767) {
            document.body.classList.add('no-scroll');
            projHeader.classList.add('index-change');
        }
        else {
            document.body.classList.remove('no-scroll');
            projHeader.classList.remove('index-change');
        }

        if ((window.innerWidth <= 1000 && window.innerHeight <= 767) || (window.innerWidth <= 767 && window.innerHeight >= 767)) {
            projHeader.classList.add('blur');
        }
        else {
            projHeader.classList.remove('blur');
        }

        if (window.innerWidth >= 767 && window.innerWidth <= 1000 && window.innerHeight >= 767) {
            projHeader.firstElementChild.classList.add('hide');
        }
        else {
            projHeader.firstElementChild.classList.remove('hide');
        }
    }
}

if (!cssdisabled) {
    document.querySelector('#cross').addEventListener('click', function(event) {
        onClose();
        gif.style.opacity = 1;
        if (activeLink.parentElement.parentElement.parentElement.nextElementSibling.classList.contains('hide')) {
            activeLink.parentElement.parentElement.parentElement.nextElementSibling.classList.remove('hide');
        }
        // focus gallery
            activeLink.parentElement.parentElement.parentElement.nextElementSibling.querySelector('.gallery').focus();
    });
    window.addEventListener('keyup', function(event) {
        if (event.keyCode == 27) {
            if (document.querySelector('main').classList.contains('show-project')) {
                onClose();
                gif.style.opacity = 1;
                if (activeLink.parentElement.parentElement.parentElement.nextElementSibling.classList.contains('hide')) {
                    activeLink.parentElement.parentElement.parentElement.nextElementSibling.classList.remove('hide');
                }
                // focus gallery
                    activeLink.parentElement.parentElement.parentElement.nextElementSibling.querySelector('.gallery').focus();
            }
        }
    });
}

// handle aside left filters
document.querySelectorAll('aside.left input').forEach(function(elem) {
    // elem.setAttribute('aria-checked', 'false');
    elem.onclick = function(e) {
        const projects = Array.from(document.querySelectorAll('.project'));
        let other = document.querySelector('aside.left label.selected');
        if (other) {
            other.classList.remove('selected');
            // other.previousElementSibling.setAttribute('aria-checked', 'false');
            if (other === this.previousElementSibling) {
                projects.forEach(function(proj) {
                    proj.parentElement.classList.add('hide');
                });
                return;
            }
        }
        this.nextElementSibling.classList.add('selected');
        // this.setAttribute('aria-checked', 'true');
        projects.forEach(function(project) {
            if (project.dataset.type.includes(elem.dataset.type)) {
                project.parentElement.classList.remove('hide');
            }
            else {
                project.parentElement.classList.add('hide');
            }
        });
        
        // Handle no scroll case
        if (!accessMode && scrollableElem.scrollHeight <= scrollableElem.offsetHeight) {
            document.body.classList.add('no-scroll');
        }
        else {
            if (document.body.classList.contains('no-scroll')) {
                document.body.classList.remove('no-scroll');
            }
        }

        // remove selected and aria-checked if reclick on input
        if (other && other.previousElementSibling == this) {
            // if (this.getAttribute('aria-checked') == false) {
            if (this.checked = false) {
                this.nextElementSibling.classList.add('selected');
                // this.setAttribute('aria-checked', 'true');
                this.checked = true;
            }
            else {
                this.nextElementSibling.classList.remove('selected');
                // this.setAttribute('aria-checked', 'false');
                this.checked = false;
            }
            projects.forEach(function(project) {
                if (!project.dataset.type.includes(elem.dataset.type)) {
                    project.parentElement.classList.remove('hide');
                }
            });
            if (document.body.classList.contains('no-scroll')) {
                document.body.classList.remove('no-scroll');
            }
        }

        // add scrolled class to last visible li if last one hidden
        if (!accessMode) {
            let visibleProjects = Array.from(projects);
            visibleProjects = visibleProjects.filter(checkClassHide);
            projects.forEach(function(project) {
                if (project.parentElement.classList.contains('scrolled')) {
                    project.parentElement.classList.remove('scrolled');
                }
            });
            if (projects[projects.length - 1].parentElement.classList.contains('hide')) {
                visibleProjects[visibleProjects.length - 1].parentElement.classList.add('scrolled');
            }
        }
    }
});

function checkClassHide(el) {
    if (!el.parentElement.classList.contains('hide')) {
        return el.parentElement;
    }
}

// move project type from head to content on mobile
moveProjectsDetails();
resizePortraitImg();

window.addEventListener('resize', () => {
    moveProjectsDetails();

    if (!accessMode && window.innerWidth < 314 && document.querySelector('#list > ul > li:first-of-type > div').classList.contains('focus')) {
        menuButton.classList.add('removeStroke');
    }
    else if (!accessMode && window.innerWidth > 314) {
        menuButton.classList.remove('removeStroke');
    }
});

function moveProjectsDetails() {
    const projectsTypes = document.querySelectorAll('.project-type');
    const projectsDate = Array.from(document.getElementsByTagName('time'));
    if (window.innerWidth < 600 || accessMode) {
        if (projectsTypes[0].parentElement.parentElement.classList.contains('project')) {
            projectsDate.forEach((projectDate) => {
                projectDate.parentElement.nextElementSibling.prepend(projectDate);
            });
            projectsTypes.forEach((projectType) => {
                projectType.parentElement.parentElement.nextElementSibling.prepend(projectType);
            });
        }
    }
    else {
        if (projectsTypes[0].parentElement.classList.contains('content')) {
            projectsDate.forEach((projectDate) => {
                projectDate.parentElement.previousElementSibling.prepend(projectDate);
            });
            projectsTypes.forEach((projectType) => {
                projectType.parentElement.previousElementSibling.querySelector('div').append(projectType);
            });
        }
    }
}

var isTactile = false;

if ( /Android|webOS|iPhone|iPad|Kindle|Tablet|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    isTactile = true;
}
else {
    isTactile = false;
}

function resizePortraitImg() {
    const imgs = !accessMode
    ? Array.from(document.querySelectorAll('.picture > img:first-of-type'))
    : Array.from(document.querySelectorAll('.picture img'));

    imgs.forEach((img) => {
        img.addEventListener('load', () => {
            if (img.naturalHeight > img.naturalWidth) {
                img.classList.add('portrait');
            }
        });
        img.addEventListener('error', () => {
            console.log('Some image failed to load, others may still be loading');
        })
    })
}

// function enlargeImg() {
//     const img = document.querySelectorAll('#project-container img');
//     for(let i = 0; i < img.length; i++) {
//         img[i].addEventListener("click", function() {
//             window.location = img[i].src;
//         });
//     }
// }

// EN specific
// resize first button elem in aside left
if (document.firstElementChild.lang == 'en') {
    resizeAsideLeftLabels();
    window.addEventListener('resize', () => {
        resizeAsideLeftLabels();
    });
}

function resizeAsideLeftLabels() {
    if (window.innerWidth >= 767 && window.innerHeight >= 767) {
        document.querySelector('#main-container > aside.left > fieldset div:first-of-type label').style.cssText = 'width: 154px';
        document.querySelector('#main-container > aside.left > fieldset div:nth-of-type(2) label').style.cssText = 'width: 51px';
        document.querySelector('#main-container > aside.left > fieldset div:last-of-type label').style.cssText = 'width: 98px';
    }
    else {
        document.querySelectorAll('#main-container > aside.left > fieldset div label').forEach((label) => {
            label.style.cssText = 'width: unset';
        });
    }
}

// modal specific
if (!cssdisabled) {
    // trapping focus
    function trapFocus(event) {
        if (!modal.contains(event.target)) {
            modal.focus();
        }
    };
    // disable click
    function noClick(event) {
        if (!modal.contains(event.target)) {
            event.stopPropagation();
            event.preventDefault();
        }
    };
    // remove listeners
    function removeListeners() {
        document.removeEventListener('focus', trapFocus, true);
        document.removeEventListener('click', noClick, true);
    }
}

// handle video description (show/hide)
function showVideoDesc() {
    const button = event.target.parentElement.querySelector('.media-button');
    button.nextElementSibling.classList.toggle('hide');
    button.classList.toggle('extra-bottom-margin');
    let buttonValue = button.firstChild.textContent;
    const videoName = button.firstElementChild.textContent;
    if (button.nextElementSibling.classList.contains('hide')) {
        button.setAttribute('aria-expanded', 'false');
        if (document.firstElementChild.lang == 'en') {
            buttonValue = buttonValue.replace('Hide', 'Display');
        }
        else {
            buttonValue = buttonValue.replace('Cacher', 'Afficher');
        }
    }
    else {
        button.setAttribute('aria-expanded', 'true');
        if (document.firstElementChild.lang == 'en') {
            buttonValue = buttonValue.replace('Display', 'Hide');
        }
        else {
            buttonValue = buttonValue.replace('Afficher', 'Cacher');
        }
    }
    button.textContent = buttonValue;
    const videoSpan = document.createElement('span');
    videoSpan.classList.add('sr-only');
    videoSpan.textContent = videoName;
    button.append(videoSpan);
}

