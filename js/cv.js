// mobile menu
const menuButton = document.getElementById('menu');
const menu = document.getElementById('pages');

menuButton.addEventListener("click", function(e) {
    if (menu.classList.contains('hide')) {
        this.setAttribute('aria-expanded', 'true');
    }
    else {
        this.setAttribute('aria-expanded', 'false');
    }
    menu.classList.toggle('hide');
    e.preventDefault();
    e.stopPropagation();
})

// close mobile menu on some elems focus
menuButton.onfocus = (e) => {
    if (!menu.classList.contains('hide')) {
        setTimeout(() => {
            if (!menu.classList.contains('hide')) {
                hideMenu();
            }
        }, 100)
    }
}

const accessMode = localStorage.getItem('accessible-mode') === 'true';

if (accessMode || window.innerWidth < 767 || window.innerHeight < 767) {
    document.querySelector('#top-accessible-banner li:last-of-type a').onfocus = () => {
        if (!menu.classList.contains('hide')) {
            hideMenu();
        }
    }
}

if (accessMode || window.innerWidth < 767 || window.innerHeight < 767) {
    setTimeout(() => {
        menu.lastElementChild.firstElementChild.onblur = (e) => {
            if (e.relatedTarget != menu.lastElementChild.previousElementSibling.firstElementChild) {
                hideMenu();
            }
        }
    }, 100);
}

// close mobile menu on window click
window.addEventListener('click', function(e) {
    hideMenu();
});

// close mobile menu on ESC key press
window.addEventListener('keyup', function(event) {
    if (event.keyCode == 27) {
        const menuArray = Array.from(document.querySelectorAll('#menu, #pages > li > a'));
        if (!menu.classList.contains('hide') && menuArray.some(elem => elem == document.activeElement)) {
                hideMenu();
                menuButton.focus();
        }
    }
});

function hideMenu() {
    if (!menu.classList.contains('hide')) {
        menu.classList.add('hide');
        menuButton.setAttribute('aria-expanded', 'false');
    }
}

// accessible mode
const accessModeBtn = document.getElementById('accessible-mode-btn');

window.addEventListener("DOMContentLoaded", (event) => {
    if (window.location.hash !== '') {
        const hash = String(window.location.hash.substring(1));
        if (hash == 'start') {
            window.scrollTo({ top: 0, behavior: 'auto' });
            setTimeout(function() {
                document.querySelector('h1').focus();
            }, 100);
        }
    }
});

accessModeBtn.onclick = () => {
    document.body.classList.toggle('accessible-mode');
    switchMode();
    // reload page to reset functionnalities
    location.reload();
}

if (accessMode) {
    switchMode(true);
    accessModeBtn.setAttribute('aria-pressed', 'true');
}
else {
    accessModeBtn.setAttribute('aria-pressed', 'false');
}

// back to top button
const topBtn = document.getElementById('top-btn');
topBtn.onclick = (e) => {
    e.preventDefault();
    document.firstElementChild.scrollTo({ top: 0, behavior: 'smooth' });
    setTimeout(function() {
        location.hash = topBtn.hash;
        document.querySelector('h1').focus();
        document.firstElementChild.scrollTo({top: 0, behavior: 'auto'});
    }, 100);
}
topBtn.addEventListener('focus', () => {
    document.firstElementChild.scrollTo({ top: document.firstElementChild.scrollHeight, behavior: 'auto' });
});

function switchMode(force) {
    const darkmode = localStorage.getItem('accessible-mode') === 'true';
    localStorage.setItem('accessible-mode', force || !darkmode)

    if (force || !darkmode) {
        document.body.classList.add('accessible-mode');
    }
    else {
        document.body.classList.remove('accessible-mode');
    }
}
