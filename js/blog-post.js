document.querySelector('#cross').addEventListener('click', function(event) {
    if (window.location.href.includes('home')) {
        window.location.href = window.location.origin + '/home' + event.target.dataset.url;
    }
    else {
        window.location.href = window.location.origin + event.target.dataset.url;
    }
});

document.getElementById('list').classList.add('extra-padding');

document.querySelector('h1 span:last-of-type').classList.add('sr-only-bis');

// enlargeImg();

// handle media description (show/hide)
document.querySelectorAll('.media-container').forEach((elem) => {
    if (elem.nextElementSibling) {
        if (elem.nextElementSibling.nodeName == 'FIGCAPTION' && elem.nextElementSibling.textContent) {
            elem.nextElementSibling.classList.add('extra-top-margin');
        }
    }
});

// remove doubled post title in footer
document.querySelector('footer > div').remove();

function showMediaDesc() {
    const button = event.target.parentElement.querySelector('.media-button');
    button.nextElementSibling.classList.toggle('hide');
    let buttonValue = button.firstChild.textContent;
    const mediaName = button.firstElementChild.textContent;
    if (button.nextElementSibling.classList.contains('hide')) {
        button.setAttribute('aria-expanded', 'false');
        if (document.firstElementChild.lang == 'en') {
            buttonValue = buttonValue.replace('Hide', 'Display');
        }
        else {
            buttonValue = buttonValue.replace('Cacher', 'Afficher');
        }
    }
    else {
        button.setAttribute('aria-expanded', 'true');
        if (document.firstElementChild.lang == 'en') {
            buttonValue = buttonValue.replace('Display', 'Hide');
        }
        else {
            buttonValue = buttonValue.replace('Afficher', 'Cacher');
        }
    }
    button.textContent = buttonValue;
    const mediaSpan = document.createElement('span');
    mediaSpan.classList.add('sr-only');
    mediaSpan.textContent = mediaName;
    button.append(mediaSpan);
}
