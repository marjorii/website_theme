// handle aside left filters
document.querySelectorAll('aside.left input').forEach(function(elem) {
    // elem.setAttribute('aria-checked', 'false');
    elem.onclick = function(e) {
        const projects = Array.from(document.querySelectorAll('.project-list'));
        let other = document.querySelector('aside.left label.selected');
        if (other) {
            other.classList.remove('selected');
            // other.previousElementSibling.setAttribute('aria-checked', 'false');
            if (other === this.previousElementSibling) {
                projects.forEach(function(proj) {
                    proj.parentElement.classList.remove('match');
                });
                return;
            }
        }
        this.nextElementSibling.classList.add('selected');
        // this.setAttribute('aria-checked', 'true');
        projects.forEach(function(project) {
            if (project.dataset.type.includes(elem.dataset.type)) {
                project.parentElement.classList.remove('hide');
            }
            else {
                project.parentElement.classList.add('hide');
            }
        });

        // Handle no scroll case
        if (!accessMode && scrollableElem.scrollHeight <= scrollableElem.offsetHeight) {
            document.body.classList.add('no-scroll');
        }
        else {
            if (document.body.classList.contains('no-scroll')) {
                document.body.classList.remove('no-scroll');
            }
        }

        // remove selected and aria-checked if reclick on input
        if (other && other.previousElementSibling == this) {
            // if (this.getAttribute('aria-checked') == false) {
            if (this.checked = false) {
                this.nextElementSibling.classList.add('selected');
                // this.setAttribute('aria-checked', 'true');
                this.checked = true;
            }
            else {
                this.nextElementSibling.classList.remove('selected');
                // this.setAttribute('aria-checked', 'false');
                this.checked = false;
            }
            projects.forEach(function(project) {
                if (!project.dataset.type.includes(elem.dataset.type)) {
                    project.parentElement.classList.remove('hide');
                }
            });
            if (document.body.classList.contains('no-scroll')) {
                document.body.classList.remove('no-scroll');
            }
        }
    }
});